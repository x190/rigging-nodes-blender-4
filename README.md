# Rigging Nodes

Node based rigging for blender 4.0. Based on: https://gitlab.com/AquaticNightmare/rigging_nodes

## Changelog

- v1.0.0:
    - Removed nodes "Handle Bone Groups", "Set Armature Layers".
    - Added nodes "Handle Armature Collections", "Visibility of Armature Collections", "Set Bones Colors".
    - Updated several parts of the existing code to work on Blender 4.0.

## About

The purpose of this addon is to let the user create rigs by using nodes. It allows a more non-destructive way
of creating rigs and lets the user preview how the rig changes with each node they add. The goal of this project
is to be as versatile as possible and let each rigger create and/or share their own "Node groups" to later reuse
on different rigs. This project is in very early development and still lacks a lot of functionallity and polishing.
For that same reason addon updates will probably require manually updating specific parts of the node trees.

## Instructions

1. Download the latest version (zip file) and install it inside blender (Edit -> Preferences -> Add-ons -> Install).
2. Create a new window and change the editor type to "Rigging Nodes".
3. Click on the + button to create a new node tree.
4. Use Shift+A to add nodes to the node tree (Almost all nodes will need an armature object, you can create it with Inputs -> Create Armature).
5. To visualize the rig click the "Set preview" button in the node you desire.

Tips:
- Open the side panel (N) to view extra options for the selected node. you can hide or unhide sockets from there.
- only nodes connected to the inputs of the current previewed node (colored blue) will be executed.
If you want two unconnected node paths to be executed you can use the "Execute" node.
