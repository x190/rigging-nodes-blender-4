import bpy
import bpy.types


'''
Module for marking bones and constraints created with rigging nodes for easier debugging/inspection inside blender

It overrides the bone properties panel and the bone constraints panel and adds buttons for selecting the node which modified the bone or constraint
'''


class BBN_track_subpath(bpy.types.PropertyGroup):
    value: bpy.props.StringProperty()


class BBN_track_info(bpy.types.PropertyGroup):
    path: bpy.props.CollectionProperty(type=BBN_track_subpath)
    created_in_node: bpy.props.StringProperty()
    was_created: bpy.props.BoolProperty(default=False)

    def set_tracking_info(self, tree, path, node_name):
        for x in path:
            subpath = self.path.add()
            subpath.value = x
        self.created_in_node = node_name

    def focus_track_node(self, context):
        win = context.window
        scr = win.screen
        areas3d = [area for area in scr.areas if area.type == 'NODE_EDITOR' and area.spaces.active.type == 'NODE_EDITOR' and area.spaces.active.tree_type == 'bbn_tree']
        if areas3d:
            region = [region for region in areas3d[0].regions if region.type == 'WINDOW']

        override = context.copy()
        override.update({'window': win,
                         'screen': scr,
                         'area': areas3d[0],
                         'region': region[0],
                         'scene': bpy.context.scene,
                         })

        full_list = [x.value for x in self.path] + [self.created_in_node]
        bpy.ops.bbn.focus_error_node(override, path=('$$$$'.join(full_list)))


class BBN_constraint_info(bpy.types.PropertyGroup):
    constraint_name: bpy.props.StringProperty()
    track_info: bpy.props.CollectionProperty(type=BBN_track_info)


class BBN_bone_info(bpy.types.PropertyGroup):
    bone_name: bpy.props.StringProperty()
    track_info: bpy.props.CollectionProperty(type=BBN_track_info)
    constraints: bpy.props.CollectionProperty(type=BBN_constraint_info)


class BBN_armature_info(bpy.types.PropertyGroup):
    track_info: bpy.props.CollectionProperty(type=BBN_track_info)
    bones: bpy.props.CollectionProperty(type=BBN_bone_info)

    def get_bone_track(self, bone_name):
        try:
            bone_track = next(x for x in self.bones if x.bone_name == bone_name)
            return bone_track
        except StopIteration:
            return None

    def get_constraint_track(self, bone_name, constraint_name):
        try:
            bone_track = next(x for x in self.bones if x.bone_name == bone_name)
            constraint_track = next(x for x in bone_track.constraints if x.constraint_name == constraint_name)
            return constraint_track
        except StopIteration:
            return None

    def add_tracking(self, node, path):
        track = self.track_info.add()
        track.path = path
        track.created_in_node = node.name

    def add_bone_tracking(self, bone_name, node, path):
        try:
            bone_track = next(x for x in self.bones if x.bone_name == bone_name)
        except StopIteration:
            bone_track = self.bones.add()
            bone_track.bone_name = bone_name

        track = bone_track.track_info.add()
        track.set_tracking_info(node.id_data, path, node.name)

    def rename_bone(self, bone_name, new_name):
        try:
            bone_track = next(x for x in self.bones if x.bone_name == bone_name)
        except StopIteration:
            return

        bone_track.bone_name = new_name

    def add_constraint_tracking(self, bone_name, constraint_name, node, path):
        try:
            bone_track = next(x for x in self.bones if x.bone_name == bone_name)
        except StopIteration:
            bone_track = self.bones.add()
            bone_track.bone_name = bone_name

        try:
            constraint_track = next(x for x in bone_track.constraints if x.constraint_name == constraint_name)
        except StopIteration:
            constraint_track = bone_track.constraints.add()
            constraint_track.constraint_name = constraint_name

        track = constraint_track.track_info.add()
        track.set_tracking_info(node.id_data, path, node.name)


last_possible_nodes = []


class BBN_OP_focus_bone_tracking_node(bpy.types.Operator):
    bl_idname = "bbn.focus_bone_tracking_node"
    bl_label = "Focus Node"

    bl_options = {'INTERNAL'}

    def get_possible_nodes(self, context):
        global last_possible_nodes
        last_possible_nodes = []
        bone_track = context.object.data.BBN_info.get_bone_track(context.active_bone.name)
        if bone_track:
            last_possible_nodes = [(str(i), x.created_in_node, x.created_in_node) for i, x in enumerate(bone_track.track_info)]
        return last_possible_nodes

    node: bpy.props.EnumProperty(items=get_possible_nodes)

    @classmethod
    def poll(cls, context):
        return context.object.type == 'ARMATURE' and context.active_bone

    def execute(self, context):
        bone_track = context.object.data.BBN_info.get_bone_track(context.active_bone.name)
        node_track = bone_track.track_info[int(self.node)]

        node_track.focus_track_node(context)
        return {'FINISHED'}


class BBN_OP_focus_constraint_tracking_node(bpy.types.Operator):
    bl_idname = "bbn.focus_constraint_tracking_node"
    bl_label = "Focus Node"

    bl_options = {'INTERNAL'}

    armature_name: bpy.props.StringProperty()
    bone_name: bpy.props.StringProperty()
    constraint_name: bpy.props.StringProperty()
    node: bpy.props.IntProperty()

    def execute(self, context):
        armature = bpy.data.armatures.get(self.armature_name)
        constraint_track = armature.BBN_info.get_constraint_track(self.bone_name, self.constraint_name)
        constraint_track.track_info[self.node].focus_track_node(context)
        return {'FINISHED'}


classes = [
    BBN_track_subpath,
    BBN_track_info,
    BBN_constraint_info,
    BBN_bone_info,
    BBN_armature_info,
    BBN_OP_focus_bone_tracking_node,
    BBN_OP_focus_constraint_tracking_node
]

# Before overriding the draw functions of the panels, store the originals here
# They are then called within the overriden function and restored when unregistered
original_draw_functions = {}


def constraint_draw_override(self_real, context):
    func_orig = original_draw_functions[self_real.bl_rna.name]
    const = self_real.get_constraint(context)
    layout = self_real.layout

    ans = func_orig(self_real, context)

    armature = context.pose_bone.bone.id_data
    bone = context.active_bone
    constraint_track = armature.BBN_info.get_constraint_track(bone.name, const.name)
    if constraint_track:
        if constraint_track.track_info:
            box = layout.box()
            for i, x in enumerate(constraint_track.track_info):
                op = box.operator('bbn.focus_constraint_tracking_node', icon='NODE', text=x.created_in_node)
                op.constraint_name = constraint_track.constraint_name
                op.bone_name = bone.name
                op.armature_name = armature.name
                op.node = i
        else:
            pass
            #box.label(text='NO TRACK INFO')
    else:
        pass
        #box.label(text='NOT CREATED WITH NODES')

    return ans


def bone_panel_draw_override(self, context):
    layout = self.layout
    func_orig = original_draw_functions[self.bl_rna.name]

    bone = context.active_bone
    if not bone:
        if context.pose_bone:
            bone = context.pose_bone.bone
        elif context.bone:
            bone = context.bone

    if bone:
        bone_track = bone.id_data.BBN_info.get_bone_track(bone.name)

        if bone_track:
            if bone_track.track_info:
                layout.operator_menu_enum('bbn.focus_bone_tracking_node', 'node', icon='NODE')
            else:
                pass
                #layout.label(text='NO TRACK INFO')
        else:
            pass
            #layout.label(text='NOT CREATED WITH NODES')

    return func_orig(self, context)


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)

    bpy.types.Armature.BBN_info = bpy.props.PointerProperty(type=BBN_armature_info)

    # constraints draw overrides
    for cls_name in dir(bpy.types):
        cls = getattr(bpy.types, cls_name)

        if cls_name.startswith('BONE_PT') and cls_name.endswith('Constraint'):
            original_draw_functions[cls_name] = cls.draw
            cls.draw = constraint_draw_override

    # panel draw overrides
    bone_panel = bpy.types.BONE_PT_context_bone
    original_draw_functions[bone_panel.bl_rna.name] = bone_panel.draw
    bone_panel.draw = bone_panel_draw_override

    constraints_panel = bpy.types.BONE_PT_constraints
    original_draw_functions[constraints_panel.bl_rna.name] = constraints_panel.draw
    constraints_panel.draw = bone_panel_draw_override


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)

    del bpy.types.Armature.BBN_info

    for cls_name in dir(bpy.types):
        cls = getattr(bpy.types, cls_name)

        if cls_name.startswith('BONE_PT') and cls_name.endswith('Constraint'):
            cls.draw = original_draw_functions[cls_name]

    bone_panel = bpy.types.BONE_PT_context_bone
    bone_panel.draw = original_draw_functions[bone_panel.bl_rna.name]

    constraints_panel = bpy.types.BONE_PT_constraints
    constraints_panel.draw = original_draw_functions[constraints_panel.bl_rna.name]


# when unregistering the addon, bring back the original
