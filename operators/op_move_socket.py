import bpy
from bpy.types import Operator


class BBN_OP_move_socket(Operator):
    bl_idname = "bbn.move_socket"
    bl_label = "Move Socket"

    node: bpy.props.StringProperty()
    current_index: bpy.props.IntProperty()
    new_index: bpy.props.IntProperty()
    is_output: bpy.props.BoolProperty()

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree

        node = bone_tree.nodes[self.node]
        sockets = node.outputs if self.is_output else node.inputs
        current_socket = sockets[self.current_index]
        new_socket = sockets[self.new_index]

        if current_socket.is_moveable and new_socket.is_moveable:
            sockets.move(self.new_index, self.current_index)
        return {'FINISHED'}
