from bpy.types import Operator

from ..sockets.socket import draw_times, get_value_times


class BBN_OP_edit_group(Operator):
    """Edit the group referenced by the active node (or exit the current node-group)"""
    bl_idname = "bbn.edit_group"
    bl_label = "Edit Group"

    bl_options = {'INTERNAL'}

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        space = context.space_data
        path = space.path
        node = path[-1].node_tree.nodes.active

        if hasattr(node, "node_tree") and node.select:
            if (node.node_tree):
                path.append(node.node_tree, node=node)
                draw_times.clear()
                get_value_times.clear()
                return {"FINISHED"}
        elif len(path) > 1:
            path.pop()
            draw_times.clear()
            get_value_times.clear()
        return {"CANCELLED"}
