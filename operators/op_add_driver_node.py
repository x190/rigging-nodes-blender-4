from bpy.types import Operator
import mathutils
import bpy


class BBN_OP_add_driver_node(Operator):
    bl_idname = "bbn.add_driver_node"
    bl_label = "Add Driver Node"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()
    socket_index: bpy.props.IntProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        new_node = tree.nodes.new("bbn_setup_driver")

        new_node.location = node.location + mathutils.Vector((-200, -50))

        tree.links.new(new_node.outputs[0], node.inputs[self.socket_index])

        return {'FINISHED'}
