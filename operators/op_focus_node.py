from bpy.types import Operator
import bpy


class BBN_OP_focus_node(Operator):
    bl_idname = "bbn.focus_node"
    bl_label = "Focus Node"

    bl_options = {'INTERNAL'}

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        bpy.ops.node.select_all(action='DESELECT')
        tree.nodes.active = node
        node.select = True
        bpy.ops.node.view_selected()

        return {'FINISHED'}
