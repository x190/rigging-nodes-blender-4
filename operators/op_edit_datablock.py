from bpy.types import Operator
import bpy


class BBN_OP_edit_datablock(Operator):
    '''Edit the stored datablock in this node'''
    bl_idname = "bbn.edit_datablock"
    bl_label = "Edit Datablock"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def invoke(self, context, event):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)
        clear_other_objects = not event.ctrl
        node.edit_datablock(context, clear_other_objects=clear_other_objects)

        return {'FINISHED'}
