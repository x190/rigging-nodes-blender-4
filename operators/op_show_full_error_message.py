from bpy.types import Operator
import bpy


class BBN_OP_show_full_error_message(Operator):
    bl_idname = "bbn.show_full_error_message"
    bl_label = ""

    node: bpy.props.StringProperty()

    bl_options = {'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    @ classmethod
    def description(self, context, properties):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(properties.node)
        return node.error_message_full

    def execute(self, context):
        return {'FINISHED'}
