import bpy


class BBN_OP_show_node_description(bpy.types.Operator):
    bl_idname = "bbn.show_node_description"
    bl_label = ""

    value: bpy.props.StringProperty()

    bl_options = {'INTERNAL'}

    @ classmethod
    def description(self, context, properties):
        return properties.value

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        strings = self.value.split('\n')
        layout = self.layout

        col = layout.column()
        for x in strings:
            col.label(text=x)
