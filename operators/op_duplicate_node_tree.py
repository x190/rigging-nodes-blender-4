from bpy.types import Operator
import bpy

from ..sockets.socket import draw_times, get_value_times


class BBN_OP_duplicate_node_tree(Operator):
    """Duplicates the node tree"""
    bl_idname = "bbn.duplicate_node_tree"
    bl_label = "Copy Node Tree"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)
        node_tree = node.node_tree
        copy = node_tree.copy()
        node.node_tree = copy
        node.node_tree_selection = copy

        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)
