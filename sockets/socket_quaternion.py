import bpy
import mathutils
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_quaternion_base(BBN_socketmixin):
    color = (1.0, 0.6, 0.1, 1)


class BBN_socket_quaternion_interface(BBN_quaternion_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_quaternion_socket'


class BBN_socket_quaternion(BBN_quaternion_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_quaternion_socket'
    bl_label = "Quat"

    default_value: bpy.props.FloatVectorProperty(size=4, subtype='QUATERNION', update=BBN_tree.value_updated, default=(1, 0, 0, 0))

    def value_to_string(self, value):
        if value:

            return str([round(x, 2) for x in value])
        return '(NONE)'

    def convert_value(self, value):
        if value is not None:
            return mathutils.Quaternion(value)
        return None


class BBN_socket_quaternion_array_interface(BBN_array_base, BBN_quaternion_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_quaternion_array_socket'


class BBN_socket_quaternion_array(BBN_array_base, BBN_quaternion_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_quaternion_array_socket'
    bl_label = "Quaternion Array"

    socket_class = BBN_socket_quaternion
