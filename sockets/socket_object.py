import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_object_base(BBN_socketmixin):
    color = (0.0, 1.0, 1.0, 1)


class BBN_socket_object_interface(NodeTreeInterfaceSocket, BBN_socket_interface, BBN_object_base):
    bl_socket_idname = 'BBN_object_socket'


class BBN_socket_object(NodeSocket, BBN_socket, BBN_object_base):
    bl_idname = 'BBN_object_socket'
    bl_label = "Object"

    icon = 'OBJECT_DATAMODE'

    compatible_sockets = ['BBN_pointer_socket']

    def convert_value(self, value):
        # If it's a pointer, get the datapath
        if type(value) is tuple and type(value[0]) is bpy.types.Object and type(value[1]) is str:
            return value[0]
        else:
            return value

    def get_value(self):
        value = super().get_value()
        if type(value) is tuple and type(value[0]) is bpy.types.Object and type(value[1]) is str:
            return value[0]
        return value

    def value_to_string(self, value):
        if value:
            try:
                return f'{value.name}'
            except ReferenceError:
                return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_object_array_interface(BBN_array_base, BBN_object_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_object_array_socket'


class BBN_socket_object_array(BBN_array_base, BBN_object_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_object_array_socket'
    bl_label = "Object Array"

    socket_class = BBN_socket_object
