import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_string_socket_enum(bpy.types.PropertyGroup):
    value: bpy.props.StringProperty()
    label: bpy.props.StringProperty()
    description: bpy.props.StringProperty()
    icon: bpy.props.StringProperty()

    def get_tuple(self, index):
        if not self.label and not self.description and not self.icon:
            return (self.value, self.value, self.value, self.value, index)
        return (self.value, self.label, self.description, self.icon, index)


last_enum_items = []


class BBN_enum_base(BBN_socketmixin):
    enum_items: bpy.props.CollectionProperty(type=BBN_string_socket_enum)
    default_value: bpy.props.StringProperty()
    color = (0.0, 0.3, 0.1, 1)

    def set_items(self, items):
        self.enum_items.clear()
        for x in items:
            item = self.enum_items.add()
            if type(x) == tuple:
                item.value = x[0]
                item.label = x[1]
                item.description = x[2]
                if len(x) > 3:
                    item.icon = x[3]
            else:
                item.value = x
                item.label = x
                item.description = x
                item.icon = ''

    def init_from_socket(self, node, socket):
        from_enum = [x.get_tuple(i) for i, x in enumerate(socket.enum_items)]
        self.set_items(from_enum)
        super().init_from_socket(node, socket)


class BBN_socket_enum_interface(BBN_enum_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_enum_socket'


class BBN_socket_enum(BBN_enum_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_enum_socket'
    bl_label = "Enum"

    dependent_classes = [BBN_string_socket_enum]

    def get_enum_items(self, context):
        global last_enum_items
        last_enum_items = [x.get_tuple(index) for index, x in enumerate(self.enum_items)]
        if not last_enum_items:
            last_enum_items = [('NONE', 'None', 'None')]
        return last_enum_items

    default_value: bpy.props.EnumProperty(items=get_enum_items, update=BBN_tree.value_updated)

    compatible_sockets = ['BBN_string_socket']

    def _init(self, **kwargs):
        self.set_items(kwargs.pop('items', []))
        return super()._init(**kwargs)

    def set_default_value(self, value):
        '''
        Changes the default value of the socket without re-executing the tree
        '''
        if self.enum_items:
            self.do_not_update = True
            self.default_value = value
            self.do_not_update = False
