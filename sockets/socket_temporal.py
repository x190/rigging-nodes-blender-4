import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_virtual_base(BBN_socketmixin):
    color = (0.0, 0.0, 0.0, 0.0)


class BBN_socket_add_array_interface(NodeTreeInterfaceSocket, BBN_socket_interface, BBN_virtual_base):
    bl_socket_idname = 'BBN_add_array_socket'


class BBN_socket_add_array(NodeSocket, BBN_socket, BBN_virtual_base):
    bl_idname = 'BBN_add_array_socket'
    bl_label = "-"

    def draw(self, context, layout, node, text):
        layout.label(text="---")

    def value_to_string(self, value):
        return '.'

    def value_to_string_extended(self, value):
        return '.'
