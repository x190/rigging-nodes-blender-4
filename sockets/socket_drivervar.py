import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_drivervar_base(BBN_socketmixin):
    color = (0.3, 0.3, 1.0, 1.0)


class BBN_socket_drivervar_interface(BBN_drivervar_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_drivervar_socket'


class BBN_socket_drivervar(BBN_drivervar_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_drivervar_socket'
    bl_label = "Driver Var"

    def value_to_string(self, value):
        if value:
            return value['variable_name']
        return '(NONE)'


class BBN_socket_drivervar_array_interface(BBN_array_base, BBN_drivervar_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_drivervar_array_socket'


class BBN_socket_drivervar_array(BBN_array_base, BBN_drivervar_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_drivervar_array_socket'
    bl_label = "Drivervar Array"

    socket_class = BBN_socket_drivervar
