import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin
import math


class BBN_float_array_value(bpy.types.PropertyGroup):
    value: bpy.props.FloatProperty()


class BBN_float_base(BBN_socketmixin):
    color = (0.6, 0.9, 0.4, 1)
    default_value: bpy.props.FloatProperty()

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)

        if hasattr(self, 'set_default_value'):
            self.set_default_value(socket.default_value)
        else:
            self.default_value = socket.default_value


class BBN_socket_float_interface(BBN_float_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_float_socket'


class BBN_socket_float(BBN_float_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_float_socket'
    bl_label = "Float"

    default_value: bpy.props.FloatProperty(update=BBN_tree.value_updated)

    compatible_sockets = ['BBN_int_socket']

    def value_to_string(self, value):
        if value is not None:
            return f'{value:.3f}'
        return '(NONE)'

    def _init(self, **kwargs):
        '''
        self['custom_default_value'] = kwargs.get('default_value', 0.0)
        self['_RNA_UI'] = {}
        self['_RNA_UI']['custom_default_value'] = {
            'min': kwargs.get('min', -math.inf),
            'soft_min': kwargs.get('soft_min', -math.inf),
            'max': kwargs.get('max', math.inf),
            'soft_max': kwargs.get('soft_max', math.inf),
            'description': kwargs.get('description', ''),
            'default': kwargs.get('default_value', 0.0)
        }
        '''
        return super()._init(**kwargs)


class BBN_socket_float_array_interface(BBN_array_base, BBN_float_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_float_array_socket'


class BBN_socket_float_array(BBN_array_base, BBN_float_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_float_array_socket'
    bl_label = "Float Array"

    socket_class = BBN_socket_float

    dependent_classes = [BBN_float_array_value]
    default_value: bpy.props.CollectionProperty(type=BBN_float_array_value)

    def convert_value(self, value):
        '''sets the value already serialized'''
        if value:
            return [x for x in value]
        return []
