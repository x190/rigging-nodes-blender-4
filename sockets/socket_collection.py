import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin
from ..runtime import cache_socket_variables


class BBN_collection_base(BBN_socketmixin):
    color = (0.0, 1.0, 0.0, 1)


class BBN_socket_collection_interface(BBN_collection_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_collection_socket'


class BBN_socket_collection(BBN_collection_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_collection_socket'
    bl_label = "Collection"
    shape = 'DIAMOND_DOT'

    source_object: bpy.props.PointerProperty(type=bpy.types.Object)
    data_path: bpy.props.StringProperty()

    show_extra_info = False

    def convert_value(self, value):
        '''sets the value already serialized'''
        if value:
            return (value[0], value[1], value[2])

        return (None, None, None)

    def get_real_value(self):
        val = self.get_value()
        if val:
            source_object, datapath, rna_type = val
            try:
                return source_object.path_resolve(datapath)
            except ValueError:
                return None
        return None

    def value_to_string(self, value):
        if value:
            obj, datapath, rna_type = value

            if obj:
                try:
                    return f'Collection {rna_type.name}'
                except ReferenceError:
                    return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_collection_array_interface(BBN_array_base, BBN_collection_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_collection_array_socket'


class BBN_socket_collection_array(BBN_array_base, BBN_collection_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_collection_array_socket'
    bl_label = "Collection Array"

    socket_class = BBN_socket_collection
