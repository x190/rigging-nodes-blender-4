import bpy
import mathutils
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_matrix_base(BBN_socketmixin):
    color = (0.6, 0.6, 0.1, 1)
    icon = 'CON_TRANSFORM'


class BBN_socket_matrix_interface(BBN_matrix_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_matrix_socket'


class BBN_socket_matrix(BBN_matrix_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_matrix_socket'
    bl_label = "Matrix"

    show_extra_info = True

    def convert_value(self, value):
        '''sets the value already serialized'''
        if value:
            return mathutils.Matrix(value)
        return None

    def get_self_value(self):
        '''Returns the stored serialized value'''
        data = cache_socket_variables.setdefault(self.id_data, {})
        if self in data:
            return data[self].copy()
        return None

    def get_self_default_value(self):
        '''Returns the serialized stored value'''
        return mathutils.Matrix()

    def value_to_string(self, value):
        if value:
            return '4X4 Matrix'
        return '(EMPTY)'

    def value_to_string_extended(self, value):
        if value:
            ans = ''
            for x in value:
                ans += f'{[round(y, 3) for y in x]}'
                ans += '\n'

            return ans
        return '(EMPTY)'


class BBN_socket_matrix_array_interface(BBN_array_base, BBN_matrix_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_matrix_array_socket'


class BBN_socket_matrix_array(BBN_array_base, BBN_matrix_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_matrix_array_socket'
    bl_label = "Matrix Array"

    socket_class = BBN_socket_matrix
