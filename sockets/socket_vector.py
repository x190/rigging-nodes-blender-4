import bpy
import mathutils
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_vector_base(BBN_socketmixin):
    color = (0.8, 0.8, 0.0, 1)

    default_value: bpy.props.FloatVectorProperty()

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)

        if hasattr(self, 'set_default_value'):
            self.set_default_value(socket.default_value)
        else:
            self.default_value = socket.default_value


class BBN_socket_vector_interface(NodeTreeInterfaceSocket, BBN_socket_interface, BBN_vector_base):
    bl_socket_idname = 'BBN_vector_socket'


class BBN_socket_vector(NodeSocket, BBN_socket, BBN_vector_base):
    bl_idname = 'BBN_vector_socket'
    bl_label = "Vector"

    default_value: bpy.props.FloatVectorProperty(update=BBN_tree.value_updated)

    def value_to_string(self, value):
        if value:
            return str([round(x, 2) for x in value])
        return '(EMPTY)'

    def value_to_string_extended(self, value):
        if value:
            return str([round(x, 2) for x in value])
        return '(EMPTY)'

    def convert_value(self, value):
        if value:
            return mathutils.Vector(value)
        return None

    def get_self_value(self):
        '''Returns the stored serialized value'''
        data = cache_socket_variables.setdefault(self.id_data, {})
        if self in data:
            return data[self].copy()
        return None

    def get_self_default_value(self):
        '''Returns the serialized stored value'''
        return mathutils.Vector((self.default_value[0], self.default_value[1], self.default_value[2]))


class BBN_socket_vector_array_interface(BBN_array_base, BBN_vector_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_vector_array_socket'


class BBN_socket_vector_array(BBN_array_base, BBN_vector_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_vector_array_socket'
    bl_label = "Vector Array"

    socket_class = BBN_socket_vector
