import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_int_array_value(bpy.types.PropertyGroup):
    value: bpy.props.IntProperty()


class BBN_int_base(BBN_socketmixin):
    color = (0.0, 0.8, 0.6, 1)

    default_value: bpy.props.IntProperty()

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)

        if hasattr(self, 'set_default_value'):
            self.set_default_value(socket.default_value)
        else:
            self.default_value = socket.default_value


class BBN_socket_int_interface(BBN_int_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_int_socket'


class BBN_socket_int(BBN_int_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_int_socket'
    bl_label = "Integer"

    default_value: bpy.props.IntProperty(update=BBN_tree.value_updated)

    @property
    def compatible_sockets(self):
        # TODO: maybe it's better to just add the sockets manually
        from ..sockets import array_sockets
        return ['BBN_float_socket'] + [name for name in array_sockets.keys()]

    def convert_value(self, value):
        if type(value) is list:
            return len(value)
        if value is not None:
            return int(value)

        return 0

    def get_value(self):
        ans = super().get_value()
        if ans is not None:
            return int(ans)
        return ans


class BBN_socket_int_array_interface(BBN_array_base, BBN_int_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_int_array_socket'


class BBN_socket_int_array(BBN_array_base, BBN_int_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_int_array_socket'
    bl_label = "Int Array"

    socket_class = BBN_socket_int

    dependent_classes = [BBN_int_array_value]
    default_value: bpy.props.CollectionProperty(type=BBN_int_array_value)
