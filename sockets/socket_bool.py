import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin
from ..runtime import cache_socket_variables


class BBN_bool_array_value(bpy.types.PropertyGroup):
    value: bpy.props.BoolProperty()


class BBN_bool_base(BBN_socketmixin):
    color = (0.7, 0.0, 0.0, 1)
    default_value: bpy.props.BoolProperty()

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)
        if hasattr(self, 'set_default_value'):
            self.set_default_value(socket.default_value)
        else:
            self.default_value = socket.default_value


class BBN_socket_bool_interface(BBN_bool_base, BBN_socket_interface, NodeTreeInterfaceSocket,):
    bl_socket_idname = 'BBN_bool_socket'


compatible_sockets = compatible_sockets = [
    'BBN_string_socket',
    'BBN_bone_socket',
    'BBN_enum_socket',

    'BBN_int_socket',
    'BBN_float_socket',

    'BBN_object_socket',
    'BBN_object_ref_socket',

    'BBN_pointer_socket',
    'BBN_driver_socket',
]


class BBN_socket_bool(BBN_bool_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_bool_socket'
    bl_label = "Bool"

    default_value: bpy.props.BoolProperty(update=BBN_tree.value_updated)

    @property
    def compatible_sockets(self):
        from..sockets import array_sockets

        return compatible_sockets + list(array_sockets.keys())

    def convert_value(self, value):
        return bool(value)

    def value_to_string(self, value):
        return str(bool(value))


class BBN_socket_bool_array_interface(BBN_array_base, BBN_bool_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_bool_array_socket'


class BBN_socket_bool_array(BBN_array_base, BBN_bool_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_bool_array_socket'
    bl_label = "Bool Array"

    has_array_default_value = True

    socket_class = BBN_socket_bool

    dependent_classes = [BBN_bool_array_value]
    default_value: bpy.props.CollectionProperty(type=BBN_bool_array_value)
