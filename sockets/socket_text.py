import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_text_base(BBN_socketmixin):
    color = (1.0, 1.9, 0.0, 1)
    shape = 'DIAMOND_DOT'


class BBN_socket_text_interface(BBN_text_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_text_socket'


class BBN_socket_text(BBN_text_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_text_socket'
    bl_label = "Text"

    default_value: bpy.props.PointerProperty(type=bpy.types.Text, update=BBN_tree.value_updated)

    icon = 'TEXT'

    def value_to_string(self, value):
        if value:
            try:
                return f'{value.name}'
            except ReferenceError:
                return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_text_array_interface(BBN_array_base, BBN_text_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_text_array_socket'


class BBN_socket_text_array(BBN_array_base, BBN_text_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_text_array_socket'
    bl_label = "Text Array"

    socket_class = BBN_socket_text
