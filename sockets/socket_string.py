import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_string_base(BBN_socketmixin):
    color = (1, 0.4, 0.9, 1)

    default_value: bpy.props.StringProperty()


class BBN_socket_string_interface(BBN_string_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_string_socket'


class BBN_socket_string(BBN_string_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_string_socket'
    bl_label = "String"

    default_value: bpy.props.StringProperty(update=BBN_tree.value_updated)

    compatible_sockets = ['BBN_bone_socket', 'BBN_float_socket', 'BBN_int_socket', 'BBN_bool_socket', 'BBN_pointer_socket', 'BBN_enum_socket']

    icon = 'SYNTAX_OFF'

    def get_value(self):
        ans = super().get_value()
        return str(ans)

    def convert_value(self, value):
        if type(value) is tuple and type(value[0]) is bpy.types.Object and type(value[1]) is str:
            return value[1]
        else:
            return str(value)

    def value_to_string(self, value):
        if value:
            return str(value)
        return '(EMPTY)'


class BBN_socket_string_array_interface(BBN_array_base, BBN_string_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_string_array_v2_socket'


class BBN_socket_string_array(BBN_array_base, BBN_string_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_string_array_v2_socket'
    bl_label = "String Array"

    socket_class = BBN_socket_string
