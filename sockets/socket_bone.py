import bpy
from bpy.types import NodeSocket, NodeTreeInterfaceSocket
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_bone_base(BBN_socketmixin):
    color = (1, 0.4, 0.9, 1)


class BBN_socket_bone_interface(BBN_bone_base, BBN_socket_interface, NodeTreeInterfaceSocket):
    bl_socket_idname = 'BBN_bone_socket'


class BBN_socket_bone(BBN_bone_base, BBN_socket, NodeSocket):
    '''Works like a string socket, but if the node has an object socket named "Armature"  and it has no links' a bone search will show up'''
    bl_idname = 'BBN_bone_socket'
    bl_label = "Bone"

    value: bpy.props.StringProperty()
    default_value: bpy.props.StringProperty(update=BBN_tree.value_updated)

    compatible_sockets = ['BBN_string_socket']

    icon = 'BONE_DATA'

    def value_to_string(self, value):
        if value:
            return value
        return '(EMPTY)'

    def draw_default_prop(self, context, layout):
        armature_socket = self.node.inputs.get('Armature')
        if not armature_socket:
            armature_socket = self.node.inputs.get('Object')
        armature = None
        if armature_socket:
            armature = armature_socket.get_value()
        if armature:
            try:
                data = armature.data
                if armature.mode == 'EDIT':
                    layout.prop_search(self, 'default_value', data, 'edit_bones', text='', icon=self.icon)
                else:
                    layout.prop_search(self, 'default_value', data, 'bones', text='', icon=self.icon)
            except ReferenceError:
                layout.prop(self, 'default_value', text='', icon=self.icon)
        else:
            layout.prop(self, 'default_value', text='', icon=self.icon)
