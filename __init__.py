import bpy

from . import node_tree
from . import sockets
from . import nodes
from . import operators
from . import ui
from . import inspection
from . import presets
from . import tree_inspection


bl_info = {
    "name": "Rigging nodes for Blender 4",
    "description": "Node based rigging",
    "author": "AquaticNightmare - Xin",
    "blender": (4, 0, 0),
    "version": (1, 0, 0),
    "category": "Rigging",
    "location": "Editor -> Rigging nodes",
    "doc_url": "https://gitlab.com/x190/rigging-nodes-blender-4",
    "tracker_url": "https://gitlab.com/x190/rigging-nodes-blender-4",
}


class BBN_preferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    deform_flag: bpy.props.StringProperty(default='DEF', name='Deform Flag')
    mechanism_flag: bpy.props.StringProperty(default='MCH', name='Mechanism Flag')
    control_flag: bpy.props.StringProperty(default='CTRL', name='Control Flag')
    target_flag: bpy.props.StringProperty(default='TRGT', name='Target Flag')

    separator: bpy.props.StringProperty(default='.', name='Separator')

    left_flag: bpy.props.StringProperty(default='L', name='Left Flag')
    right_flag: bpy.props.StringProperty(default='R', name='Right Flag')
    mid_flag: bpy.props.StringProperty(default='', name='Mid Flag')

    warning_behaviour: bpy.props.EnumProperty(name='Warning Behaviour', items=[
        ('module', 'Show Once', 'Show warnings the first time it is generated'),
        ('error', 'Throw Error', 'The node will throw an error instead of a warning'),
        ('ignore', 'Ignore All', 'No warnings will appear'),
        ('always', 'Show All Warnings', 'Show All Warnings'),
    ])

    def draw(self, context):
        '''Addon preferences will go here'''
        # TODO: add preferences for socket colors
        layout = self.layout

        main_box = layout.box()
        main_box.prop(self, 'warning_behaviour')

        main_box = layout.box()
        main_box.label(text='Naming Options:')
        main_row = main_box.row()
        main_row.separator()
        main_col = main_row.column()
        box = main_col.box()
        col = box.column(align=True)
        col.prop(self, 'deform_flag')
        col.prop(self, 'target_flag')
        col.prop(self, 'mechanism_flag')
        col.prop(self, 'control_flag')

        box = main_col.box()
        col = box.column(align=True)
        # col.prop(self, 'mid_flag')
        col.prop(self, 'right_flag')
        col.prop(self, 'left_flag')

        main_col.prop(self, 'separator')


def init_keymaps():
    kc = bpy.context.window_manager.keyconfigs.addon
    km = kc.keymaps.new(name="Node Generic", space_type='NODE_EDITOR')
    kmi = [
        km.keymap_items.new("bbn.generate_rig", 'E', 'PRESS'),
        km.keymap_items.new("bbn.group_nodes", 'G', 'PRESS', ctrl=True),
        km.keymap_items.new("bbn.edit_group", 'TAB', 'PRESS'),
        # km.keymap_items.new("bbn.add_node_frame", 'J', 'PRESS', ctrl=True)
    ]
    return km, kmi


addon_keymaps = []


def register():
    from bpy.utils import register_class

    register_class(BBN_preferences)

    node_tree.register()
    sockets.register()
    nodes.register()
    operators.register()
    ui.register()
    inspection.register()
    presets.register()
    tree_inspection.register()

    if (not bpy.app.background):
        km, kmi = init_keymaps()
        for k in kmi:
            k.active = True
            addon_keymaps.append((km, k))


def unregister():
    from bpy.utils import unregister_class

    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    inspection.unregister()
    ui.unregister()
    operators.unregister()
    nodes.unregister()
    sockets.unregister()
    node_tree.unregister()
    presets.unregister()
    tree_inspection.unregister()

    unregister_class(BBN_preferences)
