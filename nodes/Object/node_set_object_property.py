import bpy
from bpy.types import Node

from ..node_set_property import BBN_node_set_property

from mathutils import Vector

import logging


class BBN_node_set_object_property(Node, BBN_node_set_property):
    bl_idname = 'bbn_set_object_property_node'
    bl_label = "Set Object Property"

    node_version = 3

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),  # only single supported for now
    ]

    def updated_enum(self, context):
        self.update_behaviour_sockets()

    object_type: bpy.props.EnumProperty(items=[
        ('OBJECT', 'Object', 'Object', 'OBJECT_DATAMODE', 0),
        ('CURVE', 'Curve', 'Curve', 'OUTLINER_OB_CURVE', 1),
        ('ARMATURE', 'Armature', 'Armature', 'OUTLINER_OB_ARMATURE', 2),
        ('GPENCIL', 'GPencil', 'Grease Pencil', 'OUTLINER_OB_GREASEPENCIL', 3),
        ('MESH', 'Mesh', 'Mesh', 'OUTLINER_OB_MESH', 4),
    ], update=updated_enum)

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'object_type', text='')

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            inputs = self.inputs[1:]
            for x in inputs:
                if x.hide:
                    self.inputs.remove(x)
                else:
                    x.is_deletable = True

        if self.current_node_version == 2:
            self['object_type'] = self['current_behaviour']
            del self['current_behaviour']

        self.current_node_version = self.node_version

    @ property
    def props_class(self):
        if self.object_type == 'OBJECT':
            return bpy.types.Object
        elif self.object_type == 'CURVE':
            return bpy.types.Curve
        elif self.object_type == 'ARMATURE':
            return bpy.types.Armature
        elif self.object_type == 'MESH':
            return bpy.types.Mesh
        elif self.object_type == 'GPENCIL':
            return bpy.types.GreasePencil

    def process(self, context, id, path):
        obj = self.get_input_value('Object', required=True)

        if self.object_type == 'OBJECT':
            self.set_props(obj, obj)
        else:
            if self.object_type != obj.type:
                raise ValueError('The input object is not the correct type')
            self.set_props(obj, obj.data)

        self.outputs['Object'].set_value(obj)
