import bpy
from bpy.types import Node
import operator

from ..node_base import BBN_node

import mathutils
import functools


functions_map = {
    '+': lambda *args: functools.reduce(operator.add, args),
    '*': operator.mul,
    '[]': lambda a, i, j: operator.getitem(a, slice(i, j)),
    '[:]': lambda a, i, j=None: operator.getitem(a, slice(i, j)),
    'in': operator.contains,
    'not_in': lambda a, b: not operator.contains(a, b),
    'replace': str.replace
}


functions_enum = [
    (x, x.replace('_', ' ').title(), x.replace('_', ' ').title()) for x in functions_map.keys()
]


class BBN_node_string_operator(Node, BBN_node):
    bl_idname = 'bbn_node_string_operator'
    bl_label = "String Operator"
    bl_icon = 'SYNTAX_OFF'

    bl_width_default = 250

    behaviour_enum = functions_enum

    input_sockets = {
        'String': {'type': 'BBN_string_socket'}
    }

    behaviour_opt_sockets = {
        '[:]': {
            'INPUTS': {
                'End': {'type': 'BBN_int_socket'},
            }
        },
    }

    @property
    def predefined_socket(self):
        if self.current_behaviour == '+':
            return {'type': 'BBN_string_socket'}
        return ''

    behaviour_sockets = {
        '+': {
            'INPUTS': {
                'Other': {'type': 'BBN_string_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_string_socket'},
            }
        },
        '*': {
            'INPUTS': {
                'Times': {'type': 'BBN_int_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_string_socket'},
            }
        },
        '[]': {
            'INPUTS': {
                'Item': {'type': 'BBN_int_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_string_socket'},
            }
        },
        '[:]': {
            'INPUTS': {
                'Start': {'type': 'BBN_int_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_string_socket'},
            }
        },
        'in': {
            'INPUTS': {
                'Value': {'type': 'BBN_string_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_bool_socket'},
            }
        },
        'not_in': {
            'INPUTS': {
                'Value': {'type': 'BBN_string_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_bool_socket'},
            }
        },
        'replace': {
            'INPUTS': {
                'Old': {'type': 'BBN_string_socket'},
                'New': {'type': 'BBN_string_socket'},
            },
            'OUTPUTS': {
                'String': {'type': 'BBN_string_socket'},
            }
        },
    }

    def draw_label(self):
        return f"String: {self.current_behaviour.replace('_', ' ').title()}"

    def process(self, context, id, path):
        args = [self.get_input_value(i) for i in range(0, len(self.inputs))]

        func = functions_map[self.current_behaviour]

        if callable(func):
            result = func(*args)
        else:
            result = getattr(args[0], self.current_behaviour)

        # if the function does not return anything, but just changes the vector...
        if self.current_behaviour in {'negate', 'normalize'}:
            result = args[0]

        if len(self.outputs) > 1:
            for i, x in enumerate(result):
                self.set_output_value(i, x)
        else:
            if result is None:
                raise ValueError('The provided arguments do not meet the requirements')
            self.set_output_value(0, result)
