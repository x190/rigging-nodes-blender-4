from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_suboject_base import BBN_nodebase_input_subobject
from ...node_tree import BBN_tree


class BBN_node_input_gpencil(Node, BBN_nodebase_input_subobject):
    bl_idname = 'bbn_input_gpencil_node'
    bl_label = "Input Grease Pencil"
    bl_icon = 'OUTLINER_OB_GREASEPENCIL'

    stored_datablock: bpy.props.PointerProperty(type=bpy.types.GreasePencil)

    def create_datablock(self, context):
        self.stored_datablock = bpy.data.grease_pencils.new(self.name)

        self.edit_datablock(context)
