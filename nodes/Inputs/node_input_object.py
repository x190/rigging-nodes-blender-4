from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_base import BBN_nodebase_input
from ...node_tree import BBN_tree


class BBN_node_input_object(Node, BBN_nodebase_input):
    bl_idname = 'bbn_input_object_node'
    bl_label = "Input Object"
    bl_icon = 'OBJECT_DATAMODE'

    stored_datablock: bpy.props.PointerProperty(type=bpy.types.Object)

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        layout.prop(self, 'stored_datablock', text='')
        # if self.stored_datablock:
        #    layout.operator('bbn.edit_datablock', text='Edit', icon=self.bl_icon).node = self.name

    def edit_datablock(self, context, clear_other_objects=True):
        if (bpy.ops.object.mode_set.poll()):
            bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.select_all(action="DESELECT")

        if clear_other_objects:
            self.id_data.clear_stored_datablocks()

        is_obj_in_scene = self.stored_datablock in bpy.context.view_layer.objects[:]
        if not is_obj_in_scene:
            bpy.context.scene.collection.objects.link(self.stored_datablock)
            self.id_data.register_preview(context, self.stored_datablock)

        self.stored_datablock.select_set(True)
        bpy.context.view_layer.objects.active = self.stored_datablock
        bpy.ops.object.mode_set(mode="OBJECT")

    def process(self, context, id, path):
        if not self.stored_datablock:
            raise ValueError('No datablock found')

        if (bpy.ops.object.mode_set.poll()):
            bpy.ops.object.mode_set(mode="OBJECT")

        bpy.context.view_layer.objects.active = None

        input_name = self.get_input_value('Name', default=self.stored_datablock.name)
        collection_name = self.get_input_value('Collection', default='')
        mode = self.get_input_value('Mode', default='NOT_SELECTED')

        obj = self.stored_datablock.copy()
        if self.stored_datablock.data:
            data_block = self.stored_datablock.data.copy()
            obj.data = data_block

        context.space_data.node_tree.register_object(context, obj, self.create_path_to_self(path), name=input_name, collection=collection_name, mode=mode, register_data=False)

        self.outputs[0].set_value(obj)
