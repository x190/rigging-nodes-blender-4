from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_base import BBN_nodebase_input
from ..node_set_property import BBN_node_set_property
from ...node_tree import BBN_tree


class BBN_node_create_armature(Node, BBN_node_set_property, BBN_nodebase_input):
    bl_idname = 'bbn_create_armature_node'
    bl_label = "Create Armature"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    input_sockets = {
        'Name': {'type': 'BBN_string_socket'},
        'Collection': {'type': 'BBN_string_socket'},
        # 'Display Axes': {'type': 'BBN_bool_socket'},
        # 'Display Names': {'type': 'BBN_bool_socket'},
    }

    props_to_ignore = {'name'}

    @property
    def props_class(self):
        return bpy.types.Armature

    def draw_buttons(self, context, layout):
        col = layout.column(align=True)
        row = col.row(align=True)
        super().draw_buttons(context, layout)

    def process(self, context, id, path):
        bpy.context.view_layer.objects.active = None

        armature_name = self.get_input_value('Name', default='')
        collection_name = self.get_input_value('Collection', default='')
        mode = self.get_input_value('Mode', default='NOT_SELECTED')

        if armature_name in bpy.data.armatures:
            bpy.data.armatures.remove(bpy.data.armatures[armature_name], do_unlink=True)
        armature = bpy.data.armatures.new(armature_name)

        armature.show_axes = self.get_input_value('Display Axes', default=False)
        armature.show_names = self.get_input_value('Display Names', default=False)

        found_armature_obj = bpy.data.objects.get(armature_name)
        armature_obj = None
        if found_armature_obj:
            if found_armature_obj.type == 'ARMATURE':
                armature_obj = found_armature_obj
                armature_obj.data = armature
            else:
                bpy.data.objects.remove(found_armature_obj, do_unlink=True)
        if not armature_obj:
            armature_obj = bpy.data.objects.new(name=armature_name, object_data=armature)

        armature_obj.animation_data_clear()

        self.set_props(armature_obj, armature_obj.data, start_index=2)

        context.space_data.node_tree.register_object(context, armature_obj, self.create_path_to_self(path), name=armature_name, collection=collection_name, mode=mode, register_data=True)

        self.outputs[0].set_value(armature_obj)
