from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets

sorted_arrays = sorted(array_sockets.items(), key=lambda item: item[1].bl_idname)


class BBN_node_get_from_array(Node, BBN_node):
    bl_idname = 'bbn_get_from_array_node'
    bl_label = "Get"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Index': {'type': 'BBN_int_socket'}
    }

    behaviour_enum = [('NONE', 'Pick', 'Pick')] + [
        (x, item.bl_label, item.bl_label) for x, item in sorted_arrays
    ]

    behaviour_sockets = {**{
        x: {
            'OUTPUTS': {
                'Item': {'type': item.socket_class.bl_idname},
            },
            'INPUTS': {
                'Array': {'type': x},
                'Index': {'type': 'BBN_int_socket'}
            }
        } for x, item in sorted_arrays}, **{
        'NONE': {
            'INPUTS': {
                'Array': {'type': 'BBN_add_array_socket'},
                'Index': {'type': 'BBN_int_socket'}
            }
        }
    }
    }

    def draw_label(self):
        if not self.inputs['Index'].connected_socket:
            val = self.inputs['Index'].get_value()
            if val == -1:
                return 'Get Last'
            elif val == 0:
                return 'Get First'
            return 'Get {}'.format(val)

        return 'Get'

    def process(self, context, id, path):
        array = self.get_input_value('Array', required=True)

        if not array:
            raise ValueError('Array is empty')

        if self.outputs:
            index = self.get_input_value('Index')
            value = array[index]
            self.set_output_value('Item', value)

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0 and connected_socket_0.bl_idname in array_sockets.keys():
                self.current_behaviour = connected_socket_0.bl_idname

        self.remove_incorrect_links()
