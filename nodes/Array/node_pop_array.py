from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_pop_array(Node, BBN_node):
    bl_idname = 'bbn_pop_array_node'
    bl_label = "Pop"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Index': {'type': 'BBN_int_socket'}
    }

    def draw_label(self):
        if not self.inputs['Index'].connected_socket:
            val = self.inputs['Index'].get_value()
            if val == -1:
                return 'Pop Last'
            elif val == 0:
                return 'Pop First'
            return 'Pop {}'.format(val)

        return 'Pop'

    def process(self, context, id, path):
        array = self.get_input_value('Array', required=True).copy()

        if not array:
            raise ValueError('Array is empty')

        if self.outputs:
            index = self.get_input_value('Index')
            value = array.pop(index)
            self.set_output_value('Item', value)
            self.set_output_value('Array', array)

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Item', {'type': incomming_class.socket_class.bl_idname})

        self.remove_incorrect_links()
