from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...sockets import array_sockets


class BBN_OP_add_predefined_socket(bpy.types.Operator):
    bl_idname = "bbn.add_predefined_socket"
    bl_label = "Add Socket"
    bl_icon = 'ADD'

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        node.create_socket(node.inputs, '', {**node.predefined_socket, **{'is_deletable': True}}, check_created=False)

        return {'FINISHED'}


class BBN_node_make_array(Node, BBN_node):
    bl_idname = 'bbn_make_array_node'
    bl_label = "Make Array"
    bl_icon = 'MOD_ARRAY'

    node_version = 2

    @property
    def predefined_socket(self):
        return {'type': array_sockets[self.outputs[0].bl_idname].socket_class.bl_idname}

    behaviour_enum = [
        (x, item.bl_label, item.bl_label) for x, item in sorted(array_sockets.items(), key=lambda item: item[1].bl_idname)
    ]

    behaviour_sockets = {x[0]: {'OUTPUTS': {'Array': {'type': x[0]}}} for x in behaviour_enum}

    dependent_classes = [BBN_OP_add_predefined_socket]

    def upgrade_node(self):
        super().upgrade_node()

        sockets = self.inputs[:]

        for x in sockets:
            if x.bl_rna.name == 'BBN_add_array_socket':
                self.inputs.remove(x)

        if 'array_socket_type' in self.keys():
            self['current_behaviour'] = next(x[4] for x in self.behaviour_enum if x[0] == self['array_socket_type'])
            del self['array_socket_type']

        self.current_node_version = self.node_version

    def process(self, context, id, path):
        array = [self.get_input_value(i) for i, x in enumerate(self.inputs)]
        self.set_output_value('Array', array)
