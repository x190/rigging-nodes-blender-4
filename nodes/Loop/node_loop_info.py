import bpy
from ..node_base import BBN_node
from ...runtime import cache_loop_index
from bpy.types import Node


class BBN_node_loop_info(Node, BBN_node):
    bl_idname = "bbn_loop_info"
    bl_label = "Loop Index"
    bl_icon = 'FILE_REFRESH'

    output_sockets = {
        'Index': {'type': 'BBN_int_socket'},
    }

    def process(self, context, id, path):
        self.set_output_value('Index', cache_loop_index[self.id_data])
