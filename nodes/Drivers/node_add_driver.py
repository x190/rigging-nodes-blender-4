import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_add_driver(Node, BBN_node):
    bl_idname = 'bbn_add_driver'
    bl_label = "Add Driver Simple"
    bl_icon = 'DRIVER'

    bl_width_default = 300

    node_version = 2

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
        'Datapath': {'type': 'BBN_string_socket'},
        'Property': {'type': 'BBN_string_socket'},
        'Source Object': {'type': 'BBN_object_socket'},
        'Source Datapath': {'type': 'BBN_string_socket'},
        'Func': {'type': 'BBN_string_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE_TARGETS', 'Multiple Targets', 'Add driver to multiple bones', '', 1),
        ('MULTIPLE_TARGETS_AND_SOURCES', 'Multiple Targets And Sources', 'Add driver to multiple bones', '', 2)
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Datapath': {'type': 'BBN_string_socket'},
                'Source Datapath': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE_TARGETS': {
            'INPUTS': {
                'Datapath': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
                'Source Datapath': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE_TARGETS_AND_SOURCES': {
            'INPUTS': {
                'Datapath': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
                'Source Datapath': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
            },
        },
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    input_to_focus = 'Object'
    focus_mode = {'POSE'}

    def draw_buttons(self, context, layout):
        layout = layout.column(align=True)
        row = layout.row(align=True)

        super().draw_buttons(context, layout)

    def init(self, context):
        super().init(context)

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    def process(self, context, id, path):
        armature = self.get_input_value("Object", required=True)
        datapath = self.get_input_value('Datapath')

        if self.current_behaviour == 'SINGLE':
            datapath = [datapath]

        prop = self.get_input_value("Property")
        if not prop:
            raise ValueError('Property name is not valid')

        source = self.get_input_value("Source Object")

        source_datapath = self.get_input_value('Source Datapath')

        if self.current_behaviour == 'SINGLE' or self.current_behaviour == 'MULTIPLE_TARGETS':
            source_datapath = [source_datapath] * len(datapath)

        func = self.get_input_value("Func")

        for i in range(0, len(datapath)):
            current_datapath = datapath[i]
            data = armature if not datapath else armature.path_resolve(current_datapath)
            current_source_datapath = source_datapath[i]

            d = data.driver_add(prop).driver

            v = d.variables.new()
            v.name = prop
            v.targets[0].id = source
            v.targets[0].data_path = current_source_datapath

            d.expression = func if func else v.name

        self.outputs['Armature'].set_value(armature)
