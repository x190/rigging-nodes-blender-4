import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_bone_to_datapath(Node, BBN_node):
    bl_idname = 'bbn_bone_to_datapath_node'
    bl_label = "Bone to datapath"
    bl_icon = 'BONE_DATA'

    node_version = 2

    input_sockets = {
        'Name': {'type': 'BBN_bone_socket'},
    }

    output_sockets = {
        'Datapath': {'type': 'BBN_string_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Change property of multiple bones', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Name': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Datapath': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Name': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Datapath': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    bone_type: bpy.props.EnumProperty(items=[('BONE', 'Bone', 'Bone'), ('EDIT', 'Edit', 'Edit'), ('POSE', 'Pose', 'Pose')])

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    def draw_buttons(self, context, layout):
        layout.prop(self, 'bone_type', text='')
        super().draw_buttons(context, layout)

    def process(self, context, id, path):
        names = self.get_input_value('Name')

        ans = []
        if self.current_behaviour == 'SINGLE':
            names = [names]

        for name in names:
            if self.bone_type == 'BONE':
                ans.append('data.bones["{}"]'.format(name))
            elif self.bone_type == 'POSE':
                ans.append('pose.bones["{}"]'.format(name))
            elif self.bone_type == 'EDIT':
                ans.append('data.edit_bones["{}"]'.format(name))

        if self.current_behaviour == 'SINGLE':
            self.set_output_value(0, ans[0])
        else:
            self.set_output_value(0, ans)
