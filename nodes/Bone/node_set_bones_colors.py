import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


class BBN_node_set_bones_colors(Node, BBN_node):
    bl_idname = 'bbn_node_set_bones_colors'
    bl_label = "Set Bones Colors"
    bl_icon = 'BONE_DATA'

    bl_width_default = 300

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Color Set': {'type': 'BBN_enum_socket',
                      'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.BoneColor.bl_rna.properties['palette'].enum_items)]
                      # ~ 'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.BoneGroup.bl_rna.properties['color_set'].enum_items)]
                      },
        'Bone': {'type': 'BBN_bone_socket', 'override_name': 'bone'}
    }

    _opt_input_sockets = {
        'active color': {'type': 'BBN_vector_socket'},
        'normal color': {'type': 'BBN_vector_socket'},
        'select color': {'type': 'BBN_vector_socket'},
        'Bones': {'type': 'BBN_string_array_v2_socket', 'override_name': 'bone'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature")

        active = self.get_input_value("active color")
        normal = self.get_input_value("normal color")
        select = self.get_input_value("select color")
        color_set = self.get_input_value("Color Set")

        bones = self.get_input_value('bone', default=[])
        if bones and type(bones) is str:
            bones = [bones]

        for bone in bones:
            pose_bone = armature.pose.bones[bone]
            pose_bone.color.palette = color_set
            if active:
                pose_bone.color.custom.active = active
            if normal:
                pose_bone.color.custom.normal = normal
            if select:
                pose_bone.color.custom.select = select

            armature.data.BBN_info.add_bone_tracking(bone, self, path)

        self.set_output_value(0, armature)
