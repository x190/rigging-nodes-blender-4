import bpy
from ..node_base import BBN_node
from ...runtime import cache_tree_portals
from bpy.types import NodeReroute, Node, Operator

last_variables = []


class BBN_OP_set_variable(Operator):
    bl_idname = "bbn.set_variable"
    bl_label = "Set variable"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    def get_variables(self, context):
        global last_variables
        node_tree = context.space_data.edit_tree
        last_variables = [(x.variable_name, x.variable_name, x.variable_name, '', index) for index, x in enumerate(node_tree.nodes) if x.bl_rna.identifier == 'bbn_set_variable']
        return last_variables

    variables: bpy.props.EnumProperty(items=get_variables)

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.active

        if hasattr(node, 'variable_name'):
            node.variable_name = self.variables
        return {'FINISHED'}


class BBN_node_get_variable(Node, BBN_node):
    bl_idname = "bbn_get_variable"
    bl_label = "Get Tree Variable"
    bl_icon = 'EXPORT'

    socket_type: bpy.props.StringProperty()
    input_node: bpy.props.StringProperty()

    def update_socket_type(self):
        node = self.find_get_node()
        if node and node.socket_type:
            self.input_node = node.name
            if self.socket_type != node.socket_type:
                self.socket_type = node.socket_type

                self.outputs.new(self.socket_type, '.')._init()

                if len(self.outputs) > 1:
                    self.outputs.remove(self.outputs[0])
        elif self.outputs:
            self.outputs.remove(self.outputs[0])

    def updated_variable_name(self, context):
        self.update_socket_type()

    variable_name: bpy.props.StringProperty(update=updated_variable_name)

    dependent_classes = [BBN_OP_set_variable]

    def draw_buttons(self, context, layout):

        super().draw_buttons(context, layout)

        row = layout.row(align=True)
        other_node = self.find_get_node()
        if other_node:
            row.operator('bbn.focus_node', text='', icon='TRACKING_REFINE_BACKWARDS').node = other_node.name

        tree = context.space_data.edit_tree
        if context.region.type == 'UI' or self == tree.nodes.active:
            row.operator_menu_enum('bbn.set_variable', 'variables', text='set variable')

        row = layout.row(align=True)
        row.prop(self, "variable_name", text="")

    def find_get_node(self):
        for x in self.id_data.nodes:
            if x.bl_rna.identifier == 'bbn_set_variable' and x.variable_name == self.variable_name:
                return x
        return None

    def draw_label(self):
        return 'Get: ' + self.variable_name

    def get_other_node(self):
        return self.id_data.nodes.get(self.input_node)
        if self.variable_name in cache_tree_portals.setdefault(self.id_data, {}):
            node = cache_tree_portals[self.id_data][self.variable_name]
        else:
            node = self.id_data.nodes.get(self.input_node)
            cache_tree_portals[self.id_data][self.variable_name] = node
        return node

    def execute_dependants(self, context, id, path):
        node = self.get_other_node()
        if not node:
            raise ValueError('Variable was not found')

        self.execute_other(context, id, path, node)

    def process(self, context, id, path):
        node = self.get_other_node()
        self.outputs[0].set_value(node.get_input_value(0))
