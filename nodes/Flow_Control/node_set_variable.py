import bpy
from ..node_base import BBN_node
from ...runtime import runtime_info

from bpy.types import NodeReroute, Node


class BBN_node_set_variable(Node, BBN_node,):
    bl_idname = "bbn_set_variable"
    bl_label = "Set Tree Variable"
    bl_icon = 'IMPORT'

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    def get_referencing_nodes(self):
        for x in self.id_data.nodes:
            if x.bl_idname == 'bbn_get_variable' and x.variable_name == self.internal_variable_name:
                yield x

    def variable_name_changed(self, context):
        if self.internal_variable_name:
            for x in self.get_referencing_nodes():
                x.variable_name = self.variable_name

        self.internal_variable_name = self.variable_name

    socket_type: bpy.props.StringProperty()

    internal_variable_name: bpy.props.StringProperty()
    variable_name: bpy.props.StringProperty(update=variable_name_changed)

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        row = layout.row(align=True)
        row.prop(self, "variable_name", text="")
        row.operator('bbn.select_referencing_nodes', text='', icon='TRACKING_REFINE_FORWARDS').node = self.name

    def draw_label(self):
        return 'Set ' + self.variable_name

    def process(self, context, id, path):
        pass

    def copy(self, other):
        self.internal_variable_name = ''
        self.variable_name = ''

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        main_input = self.inputs[0]
        connected_socket = main_input.connected_socket
        if connected_socket and main_input.bl_rna.name == 'BBN_add_array_socket':

            self.socket_type = connected_socket.bl_rna.name

            input_socket = self.inputs.new(self.socket_type, '.')._init()

            self.id_data.relink_socket(main_input, input_socket)

            self.inputs.remove(self.inputs[0])

            for x in self.get_referencing_nodes():
                x.update_socket_type()

            # self.outputs[0].socket_type = self.socket_type

        self.remove_incorrect_links()
