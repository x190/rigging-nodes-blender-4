from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


modes = [
    ('SHOW', 'Show', 'Show collection/s'),
    ('HIDE', 'Hide', 'Hide collection/s')
]

class BBN_node_show_armature_collections(Node, BBN_node):
    bl_idname = 'bbn_show_armature_collections_node'
    bl_label = "Visibility of Armature Collections"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    bl_width_default = 300

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'}
    }

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},

    }

    _opt_input_sockets = {
        'Name': {'type': 'BBN_string_socket', 'override_name': 'names'},
        'Names': {'type': 'BBN_string_array_v2_socket', 'override_name': 'names'}
    }

    mode: bpy.props.EnumProperty(items=modes)

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'mode', text='')

    def process(self, context, id, path):
        obj = self.get_input_value("Object")
        armature = obj.data

        names = self.get_input_value('names', default=[])
        if names and type(names) is str:
            names = [names]

        is_visible = (self.mode == 'SHOW')
        for name in names:
            col = armature.collections.get(name)
            col.is_visible = is_visible

        self.set_output_value(0, obj)
