import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


modes = [
    ('ADD', 'Add', 'Add bone/s to collection'),
    ('REMOVE', 'Remove', 'Remove bone/s from collection')
]

class BBN_node_edit_armature_collections(Node, BBN_node):
    bl_idname = 'bbn_node_edit_armature_collections'
    bl_label = "Handle Armature Collections"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    bl_width_default = 300

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Index': {'type': 'BBN_int_socket'},
    }

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Name': {'type': 'BBN_string_socket'}
    }

    _opt_input_sockets = {
        'Bone': {'type': 'BBN_bone_socket', 'override_name': 'bone'},
        'Bones': {'type': 'BBN_string_array_v2_socket', 'override_name': 'bone'},
    }

    mode: bpy.props.EnumProperty(items=modes)

    input_to_focus = 'Armature'
    focus_mode = {'POSE'}

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'mode', text='')

    def process(self, context, id, path):
        armature = self.get_input_value("Armature")
        bcols = armature.data.collections
        col_name = self.get_input_value("Name")
        is_add = (self.mode == 'ADD')

        created_col = False
        col = bcols.get(col_name)
        if is_add and (col is None):
            col = bcols.new(col_name)
            created_col = True

        if (not is_add) and (col is None):
            self.set_output_value(0, armature)
            return

        index = next(i for i, x in enumerate(bcols) if x.name == col_name)
        self.set_output_value('Index', index)
        bcols.active_index = index

        bones = self.get_input_value('bone', default=[])
        if bones and type(bones) is str:
            bones = [bones]

        for bone in bones:
            pose_bone = armature.pose.bones[bone]
            if is_add:
                col.assign(pose_bone)
            else:
                col.unassign(pose_bone)
            armature.data.BBN_info.add_bone_tracking(bone, self, path)

        if (not is_add) and (len(col.bones) == 0):
            bcols.remove(col)

        self.set_output_value(0, armature)
