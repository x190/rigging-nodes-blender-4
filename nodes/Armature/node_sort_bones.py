import bpy
from bpy.types import Node
from ..node_base import BBN_node


class BBN_node_sort_bones(Node, BBN_node):
    bl_idname = 'bbn_node_sort_bones'
    bl_label = "Sort Bones"

    bl_width_default = 200

    input_sockets = {
        'Armature': {'type': 'BBN_object_ref_socket'},
        'Bones': {'type': 'BBN_string_array_v2_socket'},
        'Reverse': {'type': 'BBN_bool_socket'},
        'Expression (x)': {'type': 'BBN_string_socket', 'default_value': 'x.name'}
    }

    output_sockets = {
        'Bones': {'type': 'BBN_string_array_v2_socket'},
    }

    bone_mode: bpy.props.EnumProperty(
        items=[
            ('EDIT', 'Edit', 'Edit Bone Properties'),
            ('POSE', 'Pose', 'Pose Bone Properties'),
            ('BONE', 'Bone', 'Bone Properties')
        ])

    input_to_focus = 'Armature'

    @property
    def focus_mode(self):
        if self.bone_mode == 'EDIT':
            return {'EDIT'}
        elif self.bone_mode == 'POSE':
            return {'POSE', 'OBJECT'}
        elif self.bone_mode == 'BONE':
            return {'OBJECT', 'POSE'}

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'bone_mode', text='')

    def process(self, context, id, path):
        armature = self.get_input_value('Armature')
        bone_names = self.get_input_value('Bones', default=None)
        reverse = self.get_input_value('Reverse')
        expression = self.get_input_value('Expression (x)')

        if not expression:
            raise ValueError('Expression is not valid')

        if bone_names is not None:
            if self.bone_mode == 'POSE':
                bones = [armature.pose.bones[x] for x in bone_names]
            elif self.bone_mode == 'EDIT':
                bones = [armature.data.edit_bones[x] for x in bone_names]
            elif self.bone_mode == 'BONE':
                bones = [armature.data.bones[x] for x in bone_names]
        else:
            if self.bone_mode == 'POSE':
                bones = armature.pose.bones
            elif self.bone_mode == 'EDIT':
                bones = armature.data.edit_bones
            elif self.bone_mode == 'BONE':
                bones = armature.data.bones

        # compile the expression beforehand to avoid re-compiling on each call
        compiled = compile(expression, '<string>', 'eval')
        def exp(x): return eval(compiled)
        bones.sort(key=exp, reverse=reverse)

        self.set_output_value('Bones', list(x.name for x in bones))
