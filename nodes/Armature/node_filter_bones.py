import bpy
from bpy.types import Node
from ..node_base import BBN_node


class BBN_node_filter_bones(Node, BBN_node):
    bl_idname = 'bbn_node_filter_bones'
    bl_label = "Filter Bones"

    bl_width_default = 200

    input_sockets = {
        'Armature': {'type': 'BBN_object_ref_socket'},
        'Expression (x)': {'type': 'BBN_string_socket'}
    }

    _opt_input_sockets = {
        'Bones': {'type': 'BBN_string_array_v2_socket'},
    }

    bone_mode: bpy.props.EnumProperty(
        items=[
            ('EDIT', 'Edit', 'Edit Bone Properties'),
            ('POSE', 'Pose', 'Pose Bone Properties'),
            ('BONE', 'Bone', 'Bone Properties')
        ])

    behaviour_enum = [
        ('COLLECT_ALL', 'Collect All', 'Collect All'),
        ('GET_FIRST_MATCH', 'Get First Match', 'Get the first item that matches the expression'),
    ]

    behaviour_sockets = {
        'COLLECT_ALL': {
            'OUTPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            }
        },
        'GET_FIRST_MATCH': {
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_socket'},
            }
        }
    }

    input_to_focus = 'Armature'

    @property
    def focus_mode(self):
        if self.bone_mode == 'EDIT':
            return 'EDIT'
        elif self.bone_mode == 'POSE':
            return {'POSE', 'OBJECT'}
        elif self.bone_mode == 'BONE':
            return {'OBJECT', 'POSE'}

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'bone_mode', text='')

    def process(self, context, id, path):
        armature = self.get_input_value('Armature')
        bone_names = self.get_input_value('Bones', default=None)
        expression = self.get_input_value('Expression (x)')

        if not expression:
            raise ValueError('Expression is not valid')

        if bone_names is not None:
            if self.bone_mode == 'POSE':
                bones = [armature.pose.bones[x] for x in bone_names]
            elif self.bone_mode == 'EDIT':
                bones = [armature.data.edit_bones[x] for x in bone_names]
            elif self.bone_mode == 'BONE':
                bones = [armature.data.bones[x] for x in bone_names]
        else:
            if self.bone_mode == 'POSE':
                bones = armature.pose.bones
            elif self.bone_mode == 'EDIT':
                bones = armature.data.edit_bones
            elif self.bone_mode == 'BONE':
                bones = armature.data.bones

        # compile the expression beforehand to avoid re-compiling on each call
        compiled = compile(expression, '<string>', 'eval')

        def exp(x):
            return eval(compiled, {'x': x})

        if self.current_behaviour == 'COLLECT_ALL':
            self.set_output_value('Bones', list(x.name for x in filter(exp, bones)))
        elif self.current_behaviour == 'GET_FIRST_MATCH':
            try:
                bone = next(x for x in bones if exp(x))
                ans = bone.name
            except StopIteration:
                ans = ''
            self.set_output_value('Bone', ans)
