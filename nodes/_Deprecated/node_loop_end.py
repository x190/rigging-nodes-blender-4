import bpy
from ..node_base import BBN_node

from bpy.types import Node


class BBN_node_loop_end(Node, BBN_node):
    deprecated = True
    bl_idname = "bbn_loop_end"
    bl_label = "Loop End"
    bl_icon = 'FILE_REFRESH'

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, update the loops in the file with the "Create Correct Loops" operator in the side panel (n)')