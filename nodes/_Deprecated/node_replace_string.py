import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_replace_string(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_replace_string_node'
    bl_label = "Replace String"
    bl_icon = 'SYNTAX_OFF'

    input_sockets = {
        'String': {'type': 'BBN_string_socket'},
        'Search': {'type': 'BBN_string_socket'},
        'Replace': {'type': 'BBN_string_socket'},
    }

    output_sockets = {
        'Parent': {'type': 'BBN_string_socket'},
    }

    def init(self, context):
        super().init(context)

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the string operator node')
